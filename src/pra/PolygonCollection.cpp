#include "pra/PolygonCollection.hpp"
#include <limits>

namespace pra {

std::istream& operator>>( std::istream& is, PolygonCollection& pc )
{
    std::string s;
    while ( std::getline( is, s ) ) {
        auto polygon = Polygon::parse( s );
        if ( polygon.points().size() > 2 ) {
            pc.rect_.update( polygon.rect() );
            pc.polygons_.push_back( polygon );
        }
    }
    return is;
}
}
