#include "pra/Renderer.hpp"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <set>

namespace pra {

Renderer::Renderer( uint32_t width, uint32_t height )

  : width_( width )
  , height_( height )
  , data( width_ * height_ )
{
}

void Renderer::draw_horizontal_line( uint32_t y, std::tuple<uint32_t, uint32_t> x_range )
{
    std::fill( data.data() + y * width_ + std::get<0>( x_range ), data.data() + y * width_ + std::get<1>( x_range ), uint8_t( 255 ) );
}


struct Renderer::Line {
    Line()
    {
    }
    Line( Point a, Point b )
      : top( std::ceil( std::min( a.y, b.y ) ) )
      , bottom( std::floor( std::max( a.y, b.y ) ) )
      , k( ( b.x - a.x ) / ( b.y - a.y ) )
      , x( a.x + ( top - a.y - 1 ) * k )
    {
    }

    float next()
    {
        x += k;
        return x;
    }

    uint32_t top;
    uint32_t bottom;
    float k;
    float x;
};


void Renderer::draw_polygon( const Polygon& polygon, std::vector<Line>& lines, std::vector<float>& intersections )
{
    if ( polygon.points().size() < 3 )
        return;

    if ( lines.size() < polygon.points().size() ) {
        lines.resize( polygon.points().size() );
        intersections.resize( polygon.points().size() );
    }

    size_t lines_count = 0;

    auto prev_point = &polygon.points().back();
    for ( auto& point : polygon.points() ) {
        lines[lines_count++] = Line( *prev_point, point );
        prev_point = &point;
    }

    uint32_t y_min = std::max( {0.f, std::ceil( polygon.rect().a.y )} );
    uint32_t y_max = std::max( {0.f, std::floor( polygon.rect().b.y )} );
    for ( uint32_t y = y_min; y < y_max; ++y ) {

        size_t intersections_count = 0;

        for ( size_t i = 0; i < lines_count; ++i ) {
            if ( y >= lines[i].top && y < lines[i].bottom ) {
                intersections[intersections_count++] = lines[i].next();
            }
        }

        std::sort( intersections.data(), intersections.data() + intersections_count );

        for ( size_t i = 0; i < intersections_count; ++i ) {
            std::tuple<float, float> interval{intersections[i], intersections[i]};
            if ( ++i < intersections_count ) {
                std::get<1>( interval ) = intersections[i];
            }
            draw_horizontal_line( y, std::make_tuple( static_cast<uint32_t>( std::max( {0.f, std::ceil( std::get<0>( interval ) )} ) ),
                                                      static_cast<uint32_t>( std::max( {0.f, std::floor( std::get<1>( interval ) )} ) ) ) );
        }
    }
}

void Renderer::draw_polygon_collection( const PolygonCollection& pc )
{
    std::vector<Line> lines_buffer;
    std::vector<float> intersections_buffer;
    for ( auto& polygon : pc.polygons() ) {
        draw_polygon( polygon, lines_buffer, intersections_buffer );
    }
}

void Renderer::draw_polygon( const Polygon& polygon )
{
    std::vector<Line> lines_buffer;
    std::vector<float> intersections_buffer;
    draw_polygon( polygon, lines_buffer, intersections_buffer );
}

void Renderer::present()
{
    auto p = data.data();
    for ( uint32_t y = 0; y < height_; ++y ) {
        for ( uint32_t x = 0; x < width_; ++x ) {
            if ( *p == 0 ) {
                std::cout << ' ';
            }
            else {
                std::cout << '#';
            }
            ++p;
        }
        std::cout << '\n';
    }
}

void Renderer::save( const std::string& filename ) const
{
    std::ofstream file( filename );
    file.write( reinterpret_cast<const char*>( data.data() ), data.size() );
}
}
