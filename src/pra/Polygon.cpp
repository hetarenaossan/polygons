#include "pra/Polygon.hpp"

#include <iostream>
#include <sstream>

namespace pra {

void Rect::update( Point point )
{
    if ( point.x < a.x ) {
        a.x = point.x;
    }
    if ( point.x > b.x ) {
        b.x = point.x;
    }
    if ( point.y < a.y ) {
        a.y = point.y;
    }
    if ( point.y > b.y ) {
        b.y = point.y;
    }
}

void Rect::update( Rect rect )
{
    update( rect.a );
    update( rect.b );
}

Polygon Polygon::parse( const std::string& s )
{
    std::stringstream ss( s );
    Polygon polygon;
    Point point;
    while ( ss >> point.x ) {
        if ( ss >> point.y ) {
            polygon.rect_.update( point );
            polygon.points_.push_back( point );
        }
    }
    return polygon;
}
}
