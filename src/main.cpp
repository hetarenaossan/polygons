#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>

#include "pra/Renderer.hpp"

int main( int argc, char** argv )
{
    bool display = false;
    std::string filename;
    if ( argc == 3 && std::string( argv[1] ) == "-d" ) {
        display = true;
        filename = argv[2];
    }
    else if ( argc == 2 ) {
        filename = argv[1];
    }
    else {
        std::cerr << "usage: polygons [-d] <input_file>\n";
        return 1;
    }

    if ( argc == 3 ) {
    }
    pra::PolygonCollection pc;
    {
        std::ifstream input( filename );
        if ( !input.is_open() ) {
            std::cerr << "file \"" << argv[1] << "\" not found\n";
            return 1;
        }
        input >> pc;
    }

    pra::Renderer renderer( static_cast<uint32_t>( std::max( {0.f, pc.rect().b.x} ) ), static_cast<uint32_t>( std::max( {0.f, pc.rect().b.y} ) ) );
#ifdef PROFILING
    for ( int i = 0; i < 1000000; ++i ) {
        renderer.draw_polygon_collection( pc );
    }
#else
    renderer.draw_polygon_collection( pc );
#endif
    if ( display ) {
        renderer.present();
    }
    std::stringstream ss;
    ss << filename << "_result_" << renderer.width() << 'x' << renderer.height();
    renderer.save( ss.str() );
}