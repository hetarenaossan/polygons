APP = polygons
CXX = g++
CXXFLAGS = -std=c++14 -O3 -Wall -Werror -Wextra -pedantic ${CXXFLAGS_EXTRA}
TARGET = bin/${APP}
OBJS := $(patsubst %.cpp,%.o,$(shell find src/ -name '*.cpp'))
LIBS =
INCLUDES = -Iinclude

.PHONY: all clean

all: ${TARGET}

-include $(OBJS:.o=.d)

clean:
	rm -f ${TARGET}
	rm -f ${OBJS}
	find . -name "*.o" -type f -delete
	find . -name "*.d" -type f -delete
	find . -name "*.lo" -type f -delete

%.o: %.cpp makefile
	${CXX} ${CXXFLAGS} -c ${INCLUDES} -o $@ $<
	${CXX} ${CXXFLAGS} -M ${INCLUDES} $< > $*.d
	@mv -f $*.d $*.d.tmp
	@sed -e 's|.*:|$*.o:|' < $*.d.tmp > $*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $*.d.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $*.d
	@rm -f $*.d.tmp

${TARGET}: ${OBJS}
	@mkdir -p bin
	${CXX} -o ${TARGET} ${OBJS} ${LIBS}
