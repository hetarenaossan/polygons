#!/bin/sh
make clean
make CXXFLAGS_EXTRA='-DPROFILING -fno-omit-frame-pointer -g'
perf record -g bin/polygons sample_input
perf report -g 'graph,0.5,caller'