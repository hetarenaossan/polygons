#ifndef PRA_RENDERER_HPP
#define PRA_RENDERER_HPP

#include "PolygonCollection.hpp"

#include <tuple>

namespace pra {

class Renderer {
public:
    struct Line;
    explicit Renderer( uint32_t width, uint32_t height );

    void draw_horizontal_line( uint32_t y, std::tuple<uint32_t, uint32_t> x_range );

    void draw_polygon( const Polygon& polygon );
    void draw_polygon_collection( const PolygonCollection& pc );

    uint32_t width() const
    {
        return width_;
    }

    uint32_t height() const
    {
        return height_;
    }

    void present();

    void save( const std::string& filename ) const;

private:
    void draw_polygon( const Polygon& polygon, std::vector<Line>& lines_buffer, std::vector<float>& intersections_buffer );

private:
    uint32_t width_;
    uint32_t height_;
    std::vector<uint8_t> data;
};
}

#endif // PRA_RENDERER_HPP