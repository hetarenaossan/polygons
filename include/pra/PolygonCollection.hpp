#ifndef PRA_POLYGONCOLLECTION_HPP
#define PRA_POLYGONCOLLECTION_HPP

#include "Polygon.hpp"

#include <istream>

namespace pra {

class PolygonCollection {
    friend std::istream& operator>>( std::istream& is, PolygonCollection& pc );

public:
    const std::vector<Polygon>& polygons() const
    {
        return polygons_;
    }

    const Rect& rect() const
    {
        return rect_;
    }

private:
    std::vector<Polygon> polygons_;
    Rect rect_;
};
}

#endif // PRA_POLYGONCOLLECTION_HPP