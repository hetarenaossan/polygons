#ifndef PRA_POINT_HPP
#define PRA_POINT_HPP

namespace pra {

struct Point {
    float x;
    float y;
};
}

#endif // PRA_POINT_HPP