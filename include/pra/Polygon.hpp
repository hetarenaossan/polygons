#ifndef PRA_POLYGON_HPP
#define PRA_POLYGON_HPP

#include "Point.hpp"

#include <limits>
#include <string>
#include <tuple>
#include <vector>

namespace pra {

struct Rect {
    Rect()
      : a{std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity()}
      , b{-std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity()}
    {
    }

    void update( Point );
    void update( Rect );

    Point a;
    Point b;
};

class Polygon {
public:
    static Polygon parse( const std::string& );
    const std::vector<Point>& points() const
    {
        return points_;
    }
    const Rect& rect() const
    {
        return rect_;
    }

private:
    std::vector<Point> points_;
    Rect rect_;
};
}

#endif // PRA_POLYGON_HPP